// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package server

import (
	"log"
	"os"

	"path/filepath"
)

// StorageInitialize initializes storage, creating directories as necessary
// It returns error encountered, if any.
func StorageInitialize(opts *RedctlServerOptions) error {
	log.Printf("using root server path: %s\n", opts.ServerRootPath)

	err := os.MkdirAll(opts.ServerRootPath, 0755)
	if err != nil {
		log.Printf("failed to create path %q: %s\n", opts.ServerRootPath, err)
		return err
	}

	opts.DbPath = filepath.Join(opts.ServerRootPath, "storage", "services", "redctld", "db")
	err = os.MkdirAll(opts.DbPath, 0755)
	if err != nil {
		log.Printf("failed to create path %q: %s\n", opts.DbPath, err)
		return err
	}

	opts.ImagesPath = filepath.Join(opts.ServerRootPath, "storage", "images")
	err = os.MkdirAll(opts.ImagesPath, 0755)
	if err != nil {
		log.Printf("failed to create path %q: %s\n", opts.ImagesPath, err)
		return err
	}

	opts.GraphicsPath = filepath.Join(opts.ServerRootPath, "storage", "graphics")
	err = os.MkdirAll(opts.GraphicsPath, 0755)
	if err != nil {
		log.Printf("failed to create path %q: %s\n", opts.GraphicsPath, err)
		return err
	}

	opts.DataDir = filepath.Join(opts.ServerRootPath, "usr", "share", "redfield")
	return nil
}
