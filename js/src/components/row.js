// Copyright 2019 Assured Information Security, Inc.

import React from 'react';

const style = {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    height: '100%',
};

const Row = (props) => (
    <div style={props.style ? { ...style, ...props.style } : style}>
        {props.children ? props.children : (<div />)}
    </div>
);

export default Row;
