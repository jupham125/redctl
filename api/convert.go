package api

import (
	"fmt"
	"strings"
)

// ImageFormatFromString converts image format string to image format type
// Returns image format or nil on error (with error set)
func ImageFormatFromString(format string) (ImageFormat, error) {
	format = strings.ToUpper(format)
	switch format {
	case "RAW":
		return ImageFormat_RAW, nil
	case "ISO":
		return ImageFormat_ISO, nil
	case "QCOW2":
		return ImageFormat_QCOW2, nil
	}

	return ImageFormat_UNSPECIFIED, fmt.Errorf("invalid format: %s", format)
}

// ImageFormatFromInt converts image format integer to image format type
// Returns image format or nil on error (with error set)
func ImageFormatFromInt(format int32) (ImageFormat, error) {
	val, ok := ImageFormat_name[format]
	if !ok {
		return ImageFormat_UNSPECIFIED, fmt.Errorf("invalid format: %d", format)
	}

	return ImageFormatFromString(val)
}
