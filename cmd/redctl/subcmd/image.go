// Copyright © 2018 Assured Information Security, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	imageCmd = &cobra.Command{
		Use:   "image",
		Short: "Manage virtual disk images",
		Long:  ``,
	}

	imageListCmd = &cobra.Command{
		Use:   "list",
		Short: "List virtual disk images",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("image list called")
			client := initializeRedctlClient()

			name := viper.GetString("name")
			if name != "" {
				image, err := client.ImageFind(name)
				if err != nil {
					log.Fatalf("Failed to find all images: %v", err)
				}

				log.Println(image)
			} else {
				images, err := client.ImageFindAll()
				if err != nil {
					log.Fatalf("Failed to find all images: %v", err)
				}

				for _, image := range images {
					fmt.Println(image)
				}
			}
		},
	}
)

var (
	imageCreateCmd = &cobra.Command{
		Use:   "create",
		Short: "Create empty virtual disk image",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			name := viper.GetString("name")
			size := viper.GetInt64("size")

			fmt.Println("image create called:", name, size)
			client := initializeRedctlClient()

			backingName := viper.GetString("backing-image-name")
			if backingName != "" {
				image, err := client.ImageCreateBacked(name, size, backingName)
				if err != nil {
					log.Fatalf("Failed to create backed image: %v", err)
				}

				fmt.Println(image)
			} else {
				image, err := client.ImageCreate(name, size)
				if err != nil {
					log.Fatalf("Failed to create image: %v", err)
				}

				fmt.Println(image)
			}
		},
	}
)

var (
	imageCopyCmd = &cobra.Command{
		Use:   "copy",
		Short: "Copy image in disk repository",
		Long:  `Make a deep copy of an image`,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			source := viper.GetString("source")
			destination := viper.GetString("destination")

			fmt.Println("image copy called:", source, destination)
			client := initializeRedctlClient()

			image, err := client.ImageCopy(source, destination)
			if err != nil {
				log.Fatalf("Failed to copy image: %v", err)
			}

			fmt.Println(image)

		},
	}
)

var (
	imageDownloadCmd = &cobra.Command{
		Use:   "download",
		Short: "Download image from disk repository",
		Long:  `Synchronize disks from repository`,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("image download called")
		},
	}
)

var (
	imageUploadCmd = &cobra.Command{
		Use:   "upload",
		Short: "Upload image to disk repository",
		Long:  `Synchronize disks up to repository`,
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println("image upload called")
		},
	}
)

var (
	imageRemoveCmd = &cobra.Command{
		Use:   "remove",
		Short: "Remove image",
		Long:  ``,
		PreRun: func(cmd *cobra.Command, args []string) {
			preRunFlagsFixup(cmd)
		},
		Run: func(cmd *cobra.Command, args []string) {
			name := viper.GetString("name")
			client := initializeRedctlClient()

			err := client.ImageRemove(name)
			if err != nil {
				log.Fatalf("Failed to remove image: %v", err)
			}

			log.Printf("Removed image=%s\n", name)
		},
	}
)

func init() {
	rootCmd.AddCommand(imageCmd)

	imageCmd.AddCommand(imageCopyCmd)
	imageCopyCmd.Flags().String("source", "", "Source image name (must end with .qcow2 or .raw)")
	imageCopyCmd.Flags().String("destination", "", "Destination image name (must end with .qcow2 or .raw)")

	imageCmd.AddCommand(imageCreateCmd)
	imageCreateCmd.Flags().String("backing-image-name", "", "Backing image name (must end with .qcow2 or .raw)")
	imageCreateCmd.Flags().String("name", "", "Image name (must end with .qcow2 or .raw)")
	imageCreateCmd.Flags().Int64("size", 80000000000, "Size in bytes (NOTE: use 0 when using a backing image to match backed image size)")

	imageCmd.AddCommand(imageDownloadCmd)

	imageCmd.AddCommand(imageListCmd)
	imageCmd.Flags().String("name", "", "Image name")

	imageCmd.AddCommand(imageRemoveCmd)
	imageRemoveCmd.Flags().String("name", "", "Image name")

	imageCmd.AddCommand(imageUploadCmd)
}
